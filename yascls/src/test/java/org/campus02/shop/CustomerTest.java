package org.campus02.shop;

import static org.junit.Assert.*;

import org.junit.Test;

public class CustomerTest {
	@Test
	public void testConstructor() throws Exception {
		Customer temp = new Customer();
		assertNull(temp.getOrder());
		temp.startsOrder();
		assertNotNull(temp.getOrder());
	}
}
