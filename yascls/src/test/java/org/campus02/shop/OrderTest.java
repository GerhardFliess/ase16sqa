package org.campus02.shop;

import static org.junit.Assert.*;

import org.junit.Test;


public class OrderTest {
	@Test
	public void testConstructor() throws Exception {
		Order test = new Order();
		assertEquals(0, test.getOrderLines().size());
	}

	public void testAddOrder() throws Exception {
		Order test = new Order();
		OrderLine line = new OrderLine();
		Product p = new Product();

		line.setProduct(p);
		line.setCount(1);
		test.getOrderLines().add(line);

	}

}
